﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Makes UI elements float around  */
[AddComponentMenu("Miscellaneous/Floater")]
[RequireComponent(typeof(RectTransform))]
public class FloaterUI : MonoBehaviour {

    [Tooltip("The amplitude of the oscillation on the x axis.")]
    public float amplitudeX;
    [Tooltip("The frequency of the oscillation on the x axis.")]
    public float frequencyX;
    [Tooltip("The amplitude of the oscillation on the y axis.")]
    public float amplitudeY;
    [Tooltip("The frequency of the oscillation on the y axis.")]
    public float frequencyY;



    private Vector2 neutralPosition;     // The center of oscillations.


    private RectTransform rectTransform;

    private Vector3 temp;


    void Awake() {
        // Setting up references
        rectTransform = this.GetComponent<RectTransform>();
    }

    void OnEnable () {
        neutralPosition = rectTransform.anchoredPosition;
    }


    void LateUpdate () {
        temp.x = neutralPosition.x + amplitudeX * Mathf.Sin(Time.fixedTime * Mathf.PI * frequencyX);
        temp.y = neutralPosition.y + amplitudeY * Mathf.Sin(Time.fixedTime * Mathf.PI * frequencyY);
        
        rectTransform.anchoredPosition = temp;
        
        
        // temp.x = 1f + 0.5f * Mathf.Sin(Time.fixedTime * Mathf.PI * frequencyX);
        // temp.y = 1f + 0.5f * Mathf.Sin(Time.fixedTime * Mathf.PI * frequencyY);
        // rectTransform.sizeDelta = 200 * temp;
    }




}