﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* */
[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour
{
    
    
    [Tooltip("")][Range(0, 1)]
    public float easing;



    // A reference to the player game object of this scene
    private GameObject player;


    private Vector2 newPosition;

    private Vector2 velocitySmoothing;      // Used for smoothing



    void Start() {
        // Setting up references
        player = GameObject.FindGameObjectWithTag("Player");

    }


    void LateUpdate() {
        newPosition = Vector2.SmoothDamp(this.transform.position, player.transform.position, ref velocitySmoothing, easing);

        this.transform.position = new Vector3(newPosition.x, newPosition.y, transform.position.z);

    }
}
