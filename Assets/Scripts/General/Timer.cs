﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* @@@ */
[RequireComponent(typeof(Text))]
public class Timer : MonoBehaviour
{

    [Tooltip("The duration of the timer")]
    public int countdown;

    [Range(0, 3)]
    public float timeFactor = 1f;



    private Text text;

    private float startTime;
    
    private int duration;


    void Start()
    {
        text = GetComponent<Text>();

        startTime = Time.time;
        StartCoroutine(Countdown());
    }



    IEnumerator Countdown() {
        while (duration <= countdown) {
            duration = (int) (timeFactor * (Time.time - startTime));
            text.text = (countdown - duration).ToString();
            yield return null;
        }

        Debug.Log("End of timer");
        GlobalData.sceneEventOnHold.Complete();

    }



}
