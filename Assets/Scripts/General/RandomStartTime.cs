﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Set a random offset for an animation before it starts */
[RequireComponent(typeof(Animator))]
public class RandomStartTime : MonoBehaviour
{

    void Start() {
        Animator anim = GetComponent<Animator>();
        AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);      // could replace 0 by any other animation layer index
        anim.Play(state.fullPathHash, -1, Random.Range(0f, 1f));

    }



}
