﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckAnswer : MonoBehaviour
{

    [Tooltip("A reference to the SceneEvent played when a correct answer is submitted")]
    public SceneEvent rightAnswer;

    [Tooltip("A reference to the SceneEvent played when a wrong answer is submitted")]
    public SceneEvent wrongAnswer;

    [Tooltip("A reference to the SceneEvent played when n wrong answers have been submitted")]
    public SceneEvent wrongAnswerMultiple;
    public int n;


    [Space(5)]
    public InputField part1, part2, part3;


    private int wrongAnswerCount = 0;




    public void Submit() {
        if (IsAnswerEmpty()) {
            return;
        }
        if (Check()) {
            Debug.Log("Correct");
            StartCoroutine(ScenePlayer.PlaySceneEvent(rightAnswer));
        } else {
            wrongAnswerCount++;
            if (wrongAnswerCount < n) {
                StartCoroutine(ScenePlayer.PlaySceneEvent(wrongAnswer));
            } else {
                StartCoroutine(ScenePlayer.PlaySceneEvent(wrongAnswerMultiple));
            }
        }
    }




    bool Check() {
        return (part1.text == "10") && (part2.text == "31") && (part3.text.ToUpper() == "GEN");
    }


    bool IsAnswerEmpty() {
        return (part1.text == "") || (part2.text == "") || (part3.text == "");
    }


}
