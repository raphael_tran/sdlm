﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterController : MonoBehaviour {    


    [Tooltip("The base move speed of this game object")]
    public float moveSpeed;



    private Rigidbody2D rb;
    private SpriteRenderer sprite;
    private Animator anim;


    private bool facingRight = true;    


    void Start()
    {
        // Setting up references
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();


        rb.interpolation = RigidbodyInterpolation2D.Interpolate;

    }


     void FixedUpdate() {

        Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        directionalInput.Normalize();

        Vector2 moveAmount = moveSpeed * directionalInput * Time.deltaTime;

        
        if (moveAmount != Vector2.zero) {       // If the game object is actually moving
            
            anim.SetBool("IsMoving", true);
            
            rb.MovePosition(rb.position + moveAmount);



            if ((facingRight && (directionalInput.x < 0)) || (!facingRight && (directionalInput.x > 0))) {
                Flip();
            }


        } else {        // Not moving
            
            anim.SetBool("IsMoving", false);

        }

     }



    void Flip() {
        facingRight = !facingRight;
        Vector3 temp = transform.localScale;
        temp.x *= -1;
        transform.localScale = temp;
    }




}
