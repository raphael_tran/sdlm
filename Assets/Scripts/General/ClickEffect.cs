﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Represents an effect which is instanciated on a click */
[RequireComponent(typeof(AudioSource))]
public class ClickEffect : MonoBehaviour
{

    [Tooltip("The time waited after instanciating a click effect before destroying it")]
    public float lifetime;




    public void InstantiateHere() {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            GameObject effect = Instantiate(this.gameObject);
            position.z = 0;
            effect.transform.position = position;
    }



    public void Instantiate(RectTransform parent) {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
            GameObject effect = Instantiate(this.gameObject);
            effect.transform.SetParent(parent, false);

            position.z = 0;
            effect.transform.position = position;
    }




    void Start() {
        AudioClip audio = GetComponent<AudioSource>().clip;
        if (audio) {
            lifetime = audio.length;
        }

        StartCoroutine(WaitBeforeDestroying());
    }




    IEnumerator WaitBeforeDestroying() {
        yield return new WaitForSeconds(lifetime);
        Destroy(this.gameObject);
    }


}
