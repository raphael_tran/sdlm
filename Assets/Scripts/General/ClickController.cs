﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/* The class which manages the effects happening on click. */
public class ClickController : MonoBehaviour
{




    [Tooltip("A reference to the GameObject that will be instanciated on a click.\nIt must have its own AudioSource to have a click sound.")]
    public ClickEffect onClickEffect;
    [Tooltip("A reference to the parent UI GameObject which will contain the instanciated effects")]
    public RectTransform parent;


/*
    void Awake() {
        GameObject[] clickControllers = GameObject.FindGameObjectsWithTag("ClickController");
        if (clickControllers.Length > 1) {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
*/



    void Update() {
        if (GlobalData.explosionMode && Input.GetButtonDown("Fire1")) {
            onClickEffect.Instantiate(parent);
            //InstanciateClickEffect();
        } 

    }


    /* Instanciate the click effect associated with this script on the mouse position */
    public void InstanciateClickEffect() {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
            GameObject effect = Instantiate(onClickEffect.gameObject);
            effect.transform.SetParent(parent, false);
            
            position.z = 0;
            effect.transform.position = position;
    }



    /* Useless */
    private bool IsPointerOverUIObject() {

         PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current) {
            position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
         };
         
         List<RaycastResult> results = new List<RaycastResult>();
         EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

         return (results.Count > 0);        // 1 to account for the background being a UI Element
     }



}
