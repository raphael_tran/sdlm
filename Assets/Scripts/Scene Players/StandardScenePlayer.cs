﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * A standard version of ScenePlayer which look for a GameObject named "Main Events List"
 * in the current Scene and play the sequence event made of its SceneEvent children.
 * At the end, a new Scene is loaded.
 */
public class StandardScenePlayer : ScenePlayer
{

    public string nextScene;
    [Space(15)]

    [Tooltip("The starting index of the SceneEvent in the main events sequence (mainly used for debug)")]
    public int initialIndex;



    private SceneEvent[] mainEventsList;



    void Start() {
        // Setting up references
        mainEventsList = GameObject.Find("Main Events Sequence").GetComponentsInChildren<SceneEvent>();

        AudioListener.volume = GlobalData.masterVolume;

        #if !UNITY_EDITOR
            initialIndex = 0;
        #endif

        StartCoroutine(Play());
    }



    IEnumerator Play() {

        yield return PlayEventsSequence(mainEventsList, initialIndex);
        Debug.Log("End of main events sequence of this scene.");

        SceneManager.LoadScene(nextScene);
    }




}
