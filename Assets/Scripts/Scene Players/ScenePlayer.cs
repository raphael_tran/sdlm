﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Superclass which contains basic ressources to process the flow a scene. */
public class ScenePlayer : MonoBehaviour
{

    public static IEnumerator PlaySceneEvent(SceneEvent evnt) {
        evnt.Process();
        while (!evnt.isComplete) {
            yield return null;
        }
    }



    /* Process a list of SceneEvent in order; the list must at least contain one event */
    public static IEnumerator PlayEventsSequence(SceneEvent[] eventsList, int initialIndex = 0) {

        ArraySegment<SceneEvent> events = new ArraySegment<SceneEvent>(eventsList, initialIndex, eventsList.Length - initialIndex);
        int c = initialIndex;

        foreach (SceneEvent current in events) {
            Debug.Log("Processing SceneEvent n°" + c);
            current.Process();
            while (!current.isComplete) {
                yield return null;
            }
            c++;
        }

    }


    [Obsolete]
    public static IEnumerator WaitForSceneEvent(SceneEvent evnt) {
        while (!evnt.isComplete) {
            yield return null;
        }

    }



}
