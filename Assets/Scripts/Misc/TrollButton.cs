﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/*
 * Teleport a GameObject elsewhere when hovering the mouse over it. 
 * Currently used to troll the player by preventing them from clicking a button.
 */
[RequireComponent(typeof(RectTransform))]
public class TrollButton : MonoBehaviour, IPointerEnterHandler
{

    private RectTransform rectTransform;        // A reference to the RectTransform of the GameObject


    private Vector2 defaultPosition;
    private bool isInDefaultPosition;



    void Start() {
        // Setting up references
        rectTransform = GetComponent<RectTransform>();

        defaultPosition = rectTransform.anchoredPosition;
        isInDefaultPosition = true;
    }



    public void OnPointerEnter(PointerEventData pointerEventData) {
        
        if (!isInDefaultPosition) {
            rectTransform.anchoredPosition = defaultPosition;
        } else {
            rectTransform.anchoredPosition = new Vector3(500, defaultPosition.y);
        }
        isInDefaultPosition = !isInDefaultPosition;
    }



    public void OnDisable() {
        rectTransform.anchoredPosition = defaultPosition;
        Destroy(this);
    }




}