﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Automatically capitalize the text typed in an Input Field */
[RequireComponent(typeof(InputField))]
public class AutoCapitalize : MonoBehaviour
{

    InputField inputField;



    void Start() {
        inputField = GetComponent<InputField>();
    }



    public void Capitalize()
    {
        inputField.text = inputField.text.ToUpper();
    }

    
}
