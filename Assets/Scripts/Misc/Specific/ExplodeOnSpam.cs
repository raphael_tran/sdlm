﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Enables a GameObject with a Button to instantiate a ClickEffect when clicked multiple times.
 */

[RequireComponent(typeof(Button))]
public class ExplodeOnSpam : EffectOnMultipleClicks
{

    [Tooltip("A reference to the effect instanciated")]
    public ClickEffect effect;

    public Transform parent;



    protected override void Activate() {
        // Instanciating the effect
            GameObject instance = Instantiate(effect.gameObject, this.transform.position, Quaternion.identity);
            instance.transform.SetParent(parent, false);
            instance.transform.position = this.transform.position;

            // Pas beau, mais bon...
            instance.transform.localScale *= 3;

            StandardValues.TIME_BEFORE_MESSAGE_BUTTON = 0;
            StandardValues.MESSAGE_BUTTON_TIME_TO_DISAPPEAR = 0;
    }



}
