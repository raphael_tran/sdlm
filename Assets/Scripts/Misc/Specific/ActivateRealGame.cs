﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateRealGame : EffectOnMultipleClicks
{


    public ClickEffect explosion;



    protected override void Activate() {
        Debug.Log("Real Game Activated");
        GlobalData.explosionMode = true;
        ClickController clickController = GameObject.FindGameObjectWithTag("ClickController").GetComponent<ClickController>();
        clickController.onClickEffect = explosion;
    }



}
