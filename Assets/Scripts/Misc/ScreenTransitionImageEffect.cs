﻿/* Original code by Peter
http://www.console-dev.de

This code is related to an answer provided in the Unity forums at:
http://forum.unity3d.com/threads/circular-fade-in-out-shader.344816/
*/

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class ScreenTransitionImageEffect : MonoBehaviour
{

    /// Provides a shader property that is set in the inspector
    /// and a material instantiated from the shader
    public Shader shader;

    public Texture2D maskTexture;
    public Color maskColor = Color.black;
    public float maskSpread = 0;
    public bool maskInvert;

    public float maskValue;  




    private Material m_Material;

    Material material {
        get {
            if (!m_Material) {
                m_Material = new Material(shader);
                m_Material.hideFlags = HideFlags.HideAndDontSave;
            }
            return m_Material;
        }
    }



    void Start() {
        shader = Shader.Find("Hidden/ScreenTransitionImageEffect");

        // Disabling the image effect if the shader can't run on the users graphics card
        if (!shader || !(shader.isSupported)) {
            enabled = false;
        }
    }


    void OnDisable() {
        if (m_Material) {
            DestroyImmediate(m_Material);
        }
    }


    void OnRenderImage(RenderTexture source, RenderTexture destination) {
        if (!enabled) {
            Graphics.Blit(source, destination);
            return;
        }

        material.SetColor("_MaskColor", maskColor);
        material.SetFloat("_MaskValue", maskValue);
        material.SetFloat("_MaskSpread", maskSpread);
        material.SetTexture("_MainTex", source);
        material.SetTexture("_MaskTex", maskTexture);

        if (material.IsKeywordEnabled("INVERT_MASK") != maskInvert) {
            if (maskInvert)
                material.EnableKeyword("INVERT_MASK");
            else
                material.DisableKeyword("INVERT_MASK");
        }

        Graphics.Blit(source, destination, material);
    }



}
