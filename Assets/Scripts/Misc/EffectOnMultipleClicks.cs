﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Enables a GameObject with a Button to do something when clicked multiple times successively
 */

[RequireComponent(typeof(Button))]
public abstract class EffectOnMultipleClicks : MonoBehaviour
{

    [Tooltip("The number of consecutive clicks required to activate the effect")]
    public int clicksRequired;

    [Tooltip("The maximum time allowed between two click for them to be considered consecutive")]
    public float delay = 0.2f;



    protected int count = 0;
    protected float timeLastClicked;



    void Start()
    {
        GetComponent<Button>().onClick.AddListener(delegate {Click(); });

    }



    public void Click() {
        if (Time.time - timeLastClicked > delay) {
            // Reset the counter
            count = 1;
        } else {
            count++;
        }
        timeLastClicked = Time.time;

        if (count >= clicksRequired) {
            Activate();

            // Resetting
            count = 0;
        }
    }


    /* What happens when enough clicks have been made */
    protected abstract void Activate();



}
