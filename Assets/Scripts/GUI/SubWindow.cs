﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Attach this script to a GUI window that can be displayed or not. For example, a help window. */
public class SubWindow : MonoBehaviour
{

    private static Queue<SubWindow> open = new Queue<SubWindow>();       // A list of all currently opened sub windows



    /* This function can be called from any instance of SubWindow */
    public void CloseAll() {
        foreach (SubWindow window in open) {
            window.Close();
        }
        open.Clear();
    }



    public void Open(bool closeOthers = true) {
        if (closeOthers) {
            CloseAll();
        }
        open.Enqueue(this);
        this.gameObject.SetActive(true);
    }


    void Close() {
        this.gameObject.SetActive(false);
    }





}
