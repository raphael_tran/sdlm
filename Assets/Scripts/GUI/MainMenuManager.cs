﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{

    [Tooltip("A reference to the parent GameObject which contains the main GUI buttons")]
    public GameObject buttons;


    [Tooltip("A reference to the parent GameObject which contains the school selection buttons")]
    public GameObject schoolSelection;



    public void Play() {

        buttons.SetActive(false);
        //schoolSelection.SetActive(true);

        StartCoroutine(LoadIntroduction());


    }




    public void SchoolSelect(int n) {
        // 0 -> IMT-BS ; 1 -> TSP ; 2 -> Other

        // TODO: process school selected with GameAnalytics

        schoolSelection.SetActive(false);


    }
    



    IEnumerator LoadIntroduction() {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Introduction");
    }


}
