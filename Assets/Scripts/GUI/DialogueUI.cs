﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * This script must be attached to the parent GameObject of all dialogue UI elements.
 * It enables easy referencing to the individual parts.
 */
public class DialogueUI : MonoBehaviour
{

    [Tooltip("A reference to the UI Object which display the dialogue box and the text on the canvas")]
    public GameObject textDisplay;

    [Tooltip("A reference to the button which enable to advance the messages")]
    public GameObject continueButton;

    [Tooltip("A reference to the UI element which contains the buttons for a choice")]
    public GameObject choicesUI;


}
