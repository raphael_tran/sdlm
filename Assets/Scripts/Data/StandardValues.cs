﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* A common place to edit constant values which characterise the gameplay and won't change during the game. */
public static class StandardValues
{


    /* The time in second waited after the end of the message before displaying the button(s) to advance */
    // /!\ TIME_BEFORE_MESSAGE_BUTTON must be less than MESSAGE_BUTTON_TIME_TO_DISAPPEAR !!!
    public static float TIME_BEFORE_MESSAGE_BUTTON = 0.3f;

    /* The time it takes for the dialogue button(s) to disappear from the UI when disabled */
    public static float MESSAGE_BUTTON_TIME_TO_DISAPPEAR = 0.1f;


    /* The fadeout duration when cutting abruptly a music */
    public const float MUSIC_CUT_TIME = 0.47f;

    /* The pitch at which the BGM is shortly raised when cutting abruptly a music */
    public const float MUSIC_CUT_PITCH = 1.025f;


}
