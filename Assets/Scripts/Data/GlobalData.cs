﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This class contains all global values that can change and need to be accessed by any script. */
public static class GlobalData
{

    /* 
     * The standard typing speed for messages in dialogues (number of character typed per second).
     * This can be changed by the player through an option menu.
     */
    public static float defaultTypingSpeed = 100;



    [Range(0, 1)]
    public static float masterVolume = 1f;

    [Range(0, 1)]
    public static float masterVolumeBGM = 0.6f;

    [Range(0, 1)]
    public static float masterVolumeSE = 1f;


    /* Instantiate a click effect on every click. */
    public static bool explosionMode = false;


    /* The eventual WaitUntil SceneEvent pending */
    public static WaitUntil sceneEventOnHold = null;




}
