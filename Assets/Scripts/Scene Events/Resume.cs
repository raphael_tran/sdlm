﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resume : SceneEvent
{

    public WaitUntil eventOnHold;


    public override void Process() {
        eventOnHold.Complete();
        isComplete = true;
    }



}
