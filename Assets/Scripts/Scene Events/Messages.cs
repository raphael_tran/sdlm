﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Represents a sequence of simple text messages */
[AddComponentMenu("Scene Events/Dialogue/Messages")]
public class Messages: DialogueEvent
{

    public bool overrideStandardTypingSpeed;

    [Tooltip("The number of character typed per second (only if Override Standard Typing Speed is true)")]
    public float customTypingSpeed = GlobalData.defaultTypingSpeed;


    [Space(6)]
    [Tooltip("The list of differents messages written")][TextArea]
    public string[] messages;

    public bool closeDialogueUIOnEnd;



    private int indexMessage = 0;      // The index in the list of successive sentences



    protected override void Awake() {
        base.Awake();

        if (overrideStandardTypingSpeed) {
            typingSpeed = customTypingSpeed;
        }
    }



    public override void Process() {

        // Setting the continue button to advance the dialogue when clicked
        dialogueUI.continueButton.GetComponent<Button>().onClick.AddListener(NextSentence);

        // Displaying the dialogue box
        dialogueUI.textDisplay.SetActive(true);

        StartCoroutine(Type(messages[indexMessage]));
    }



    protected override IEnumerator Type(string sentence) {
        yield return base.Type(sentence);

        StartCoroutine(SetActiveButton(dialogueUI.continueButton.GetComponent<Button>(), true));
    }



    protected override IEnumerator SetActiveButton(Button button, bool value) {
        yield return base.SetActiveButton(button, value);
        button.Select();
    }



    /* The method called by the continue button to advance the dialogue. */
    public void NextSentence() {

        StartCoroutine(SetActiveButton(dialogueUI.continueButton.GetComponent<Button>(), false));

        if (indexMessage < messages.Length - 1) {        // if it remains messages to be shown

            indexMessage++;
            text.text = "";
            StartCoroutine(Type(messages[indexMessage]));

        } else {        // The whole dialogue is complete
            text.text = "";

            //
            dialogueUI.continueButton.GetComponent<Button>().onClick.RemoveListener(NextSentence);

            if (closeDialogueUIOnEnd) {
                dialogueUI.textDisplay.SetActive(false);
            }

            isComplete = true;
        }

    }



}
