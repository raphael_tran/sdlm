﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Display the black screen filter with a highlighted zone over the UI.
 * To do so, this SceneEvent set the black screen mask to some position and scale, and then show the black screen image.
 * To remove the filter through SceneEvent, you need to disable the Black Screen child GameObject.
 */
[AddComponentMenu("Scene Events/Set Highlight")]
public class SetHighlight : SceneEvent
{

    [Tooltip("A reference to the parent GameObject whcich contains the highlight filter")]
    public GameObject highlightFilter;


    [Tooltip("The center point of the element you want to highlight")]
    public Vector2 highlightCenter;

    [Tooltip("The size of the zone highlighted")]
    public Vector2 highlightZoneSize;



    private GameObject blackScreen;     // A reference to the overlay image of the filter
    private RectTransform mask;        // A reference to the SpriteMask ('s transform) which set the highlighted zone



    protected override void Awake() {
        // Setting up references
        blackScreen = highlightFilter.GetComponentInChildren<SpriteRenderer>(true).gameObject;
        mask = (RectTransform) highlightFilter.GetComponentInChildren<SpriteMask>().transform;

        happensInOneFrame = true;
    }



    public override void Process() {
        
        mask.anchoredPosition = highlightCenter;
        mask.localScale = highlightZoneSize;
        
        blackScreen.SetActive(true);

        isComplete = true;
    }



}
