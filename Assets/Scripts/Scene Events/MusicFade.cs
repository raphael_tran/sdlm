﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Enable to change progressively over time the volume of the main music.
 * This happens in parralel of the next Scene Events.
 */
[AddComponentMenu("Scene Events/Music Fade")]
public class MusicFade : SceneEvent
{

    public static bool inProgress;      // Indicates if a transition is currently taking place



    [Tooltip("The target volume relative to the master volumes")] [Range(0, 1)]
    public float targetVolume;


    [Tooltip("The time required to reach the target volume")]
    public float transitionTime;



    protected AudioSource musicPlayer;       // A reference to the main music player of the scene



    protected override void Awake() {
        base.Awake();

        // Setting up references
        musicPlayer = GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>();
    }



    public override void Process() {
        if (transitionTime > 0) {
            if (inProgress) {
                Debug.LogWarning("Multiple music fadings happening at the same time.");
            }
            inProgress = true;
            StartCoroutine(ProgressTransition(GlobalData.masterVolumeBGM * targetVolume));

        } else {
            musicPlayer.volume = GlobalData.masterVolumeBGM * targetVolume;

        }

        isComplete = true;
    }



    protected virtual IEnumerator ProgressTransition(float target) {

        float initialVolume = musicPlayer.volume;

        float t = 0;
        while (t < 1) {
            musicPlayer.volume = Mathf.SmoothStep(initialVolume, target, t);
            yield return null;

            t += Time.deltaTime / transitionTime;
        }
        musicPlayer.volume = target;

        inProgress = false;
    }



}
