﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Special SceneEvent which does not complete until a signal is sent to it.
 * It enables to "pause" the sequence of SceneEvents to do something else.
 */
public class WaitUntil : SceneEvent
{

    public override void Process() {
        // Adding this to something with global access
        GlobalData.sceneEventOnHold = this;
    }



    /* Complete the SceneEvent */
    public void Complete() {
        isComplete = true;
    }



}
