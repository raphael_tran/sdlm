﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* TODO document here */
[AddComponentMenu("Scene Events/Transition")]
public class Transition : SceneEvent
{

    [Tooltip("The time it takes to complete the whole transition effect")]
    public float duration;

    [Tooltip("The Texture which defines the shape of the transition effect")]
    public Texture2D transitionMask;

    public Color maskColor = Color.black;

    [Tooltip("How much the mask \"spreads\"; set 0 for a sharp cut")]
    public float maskSpread = 0;

    [Tooltip("Invert the black and white in the texture mask")]
    public bool maskInvert;


    [Tooltip("Check this switch fade in/out")]
    public bool direction;



    // A reference to the script attached to the main Camera which show the mask
    private ScreenTransitionImageEffect scTrImEf;



    protected override void Awake() {
        base.Awake();

        // Setting up references
        scTrImEf = GameObject.FindWithTag("MainCamera").GetComponent<ScreenTransitionImageEffect>();

    }



    public override void Process() {

        // Transfering input values
        scTrImEf.maskTexture = transitionMask;
        scTrImEf.maskColor = maskColor;
        scTrImEf.maskSpread = maskSpread;
        scTrImEf.maskInvert = maskInvert;


        StartCoroutine(ProgressTransition());
        /*
        if (!waitForCompletion) {
            isComplete = true;
        }
        */
    }


    IEnumerator ProgressTransition() {
        
        float t = (direction ? 0: 1);
        while (direction ? (t < 1): (0 < t)) {
            scTrImEf.maskValue = Mathf.SmoothStep(0, 1+scTrImEf.maskSpread, t);
            yield return null;

            t += (direction ? 1: -1) * Time.deltaTime / duration;
        }
        scTrImEf.maskValue = (direction ? 1 + scTrImEf.maskSpread: 0);


        isComplete = true;
    }

}
