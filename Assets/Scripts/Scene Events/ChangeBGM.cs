﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Change the main music which is played in the scene.
 * /!\ There is no fading here. Please use MusicFade
 */
[AddComponentMenu("Scene Events/Change BGM")]
public class ChangeBGM : SceneEvent
{

    public AudioClip newMusic;



    private AudioSource musicPlayer;       // A reference to the main music player of the scene



    protected override void Awake() {
        base.Awake();

        // Setting up references
        musicPlayer = GameObject.FindGameObjectWithTag("MainMusic").GetComponent<AudioSource>();
    }



    public override void Process() {
        musicPlayer.clip = newMusic;
        musicPlayer.Play();

        isComplete = true;
    }


}
