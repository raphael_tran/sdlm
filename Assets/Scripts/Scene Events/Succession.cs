﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Works like a "folder" for events.
 * Completing this event means completing a succession of scene events.
 * They must be set as children of this GameObject.
 */
[AddComponentMenu("Scene Events/Succession")]
public class Succession : SceneEvent
{

    SceneEvent[] succession;



    protected override void Awake()
    {
        base.Awake();

        succession = GetComponentsInChildren<SceneEvent>();     // /!\ succession[0] is this
        
        /*
        // Disabling the event in the succession in order for them to be ignored elsewhere
        for (int i = 1; i < succession.Length; i++) {
            succession[i].gameObject.SetActive(false);
        } */

    }


    public override void Process() {
        StartCoroutine(PlaySubEvents());
    }



    IEnumerator PlaySubEvents() {

        // degueu... remettre le index
        ArraySegment<SceneEvent> events = new ArraySegment<SceneEvent>(succession, 1, succession.Length - 1);
        foreach (SceneEvent current in events) {
            current.gameObject.SetActive(true);
            current.Process();
            if (current.happensInOneFrame) {
                continue;
            }
            while (!current.isComplete) {
                yield return null;
            }
        }

        isComplete = true;
    }


}
