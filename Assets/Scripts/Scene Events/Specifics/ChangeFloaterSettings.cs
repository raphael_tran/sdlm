﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Nul à iéch */
public class ChangeFloaterSettings : SceneEvent
{

    public FloaterUI floater;

    [Tooltip("The amplitude of the oscillation on the x axis.")]
    public float newAmplitudeX;
    [Tooltip("The frequency of the oscillation on the x axis.")]
    public float newFrequencyX;
    [Tooltip("The amplitude of the oscillation on the y axis.")]
    public float newAmplitudeY;
    [Tooltip("The frequency of the oscillation on the y axis.")]
    public float newFrequencyY;



    protected override void Awake() {
        happensInOneFrame = true;

    }



    public override void Process() {

        floater.amplitudeX = newAmplitudeX;
        floater.amplitudeY = newAmplitudeY;
        floater.frequencyX = newFrequencyX;
        floater.frequencyY = newFrequencyY;

        isComplete = true;
    }






}
