﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Used for the Sterling jumpscare: sets him in front of the dialogue box. */
public class ChangeSortOrder : SceneEvent
{

    public Canvas canvas;

    public int newSortingOrder;



    public override void Process() {
        Debug.Log("Sorting order: " + canvas.sortingOrder);
        canvas.sortingOrder = newSortingOrder;
        Debug.Log("Sorting order: " + canvas.sortingOrder);

        isComplete = true;
    }


}
