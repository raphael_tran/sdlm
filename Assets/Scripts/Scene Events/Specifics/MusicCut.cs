﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Used to brutally cut the music. 
 * This basically quickly fade out the music 
 */
public class MusicCut : MusicFade
{

    [TextArea]
    public string note = "Editing the fields above in the inspector does nothing.\nEdit StandardValues.cs if you want to change the music cut behaviour.";


    protected override void Awake() {
        base.Awake();

        this.targetVolume = 0;
        this.transitionTime = StandardValues.MUSIC_CUT_TIME;
    }



    protected override IEnumerator ProgressTransition(float targetVolume) {
        
        float initialPitch = musicPlayer.pitch;
        
        musicPlayer.pitch = StandardValues.MUSIC_CUT_PITCH;
        yield return base.ProgressTransition(targetVolume);
        musicPlayer.pitch = initialPitch;


        isComplete = true;

    }


}
