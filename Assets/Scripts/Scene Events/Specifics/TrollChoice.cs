﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * For the moment:
 * you need to input two choices. The button associated with the first one will "dodge" being clicked.
 */
public class TrollChoice: Choice
{

    protected TrollButton trollButton;      // A reference to the TrollButton Component which is attached to the troll button


    protected override void SetChoiceButtons(bool value) {
        base.SetChoiceButtons(value);
        if (value) {
            trollButton = buttons[0].gameObject.AddComponent<TrollButton>();
        }
    }



}
