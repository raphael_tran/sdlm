﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

/* Progressively change the transparency of a UI VisualElement over time. */
[AddComponentMenu("Scene Events/Fade Alpha")]
public class FadeAlpha : SceneEvent
{

    [Tooltip("The UI VisualElement to fade")][SerializeField]
    public VisualElement toFade;

    [Tooltip("The target transparency")][Range(0, 1)]
    public float targetAlpha;

    [Tooltip("The amount of time required to reach the target transparency")]
    public float transitionTime;


    public bool waitForCompletion;



    public override void Process() {
        if (transitionTime > 0) {
            StartCoroutine(ProgressTransition(targetAlpha));

        } else {
            
            Color temp = toFade.style.color.value;
            temp.a = targetAlpha;
            toFade.style.color = new StyleColor(temp);

        }

        isComplete = true;
    }



    protected virtual IEnumerator ProgressTransition(float target) {

        float initialAlpha = toFade.style.color.value.a;

        Color current;
        float t = 0;
        do {
            current = toFade.style.color.value;
            current.a = Mathf.SmoothStep(initialAlpha, targetAlpha, t);

            toFade.style.color = current;
            yield return null;

            t += Time.deltaTime / transitionTime;
        } while (t < 1);


        current.a = targetAlpha;
        toFade.style.color = current;

    }






}


