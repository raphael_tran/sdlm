﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This can be used to play a sound effect but also change the background music track. */
public class PlayAudio : SceneEvent
{

    public AudioClip audioClip;

    [Tooltip("A reference to the AudioSource which will play the sound")]
    public AudioSource audioSource;

    public bool loop;



    public override void Process() {
        audioSource.clip = audioClip;
        audioSource.Play();

        isComplete = true;

    }



}
