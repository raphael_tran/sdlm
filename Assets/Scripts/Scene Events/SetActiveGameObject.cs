﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Move a GameObject in the UI. */
[AddComponentMenu("Scene Events/Set Active Game Object")]
public class SetActiveGameObject : SceneEvent
{

    [Tooltip("The target GameObject to move")]
    public GameObject target;

    public enum Action { Enable, Disable, Toggle }
    public Action action;




    protected override void Awake() {
        base.Awake();
    }


    public override void Process() {
        switch (action) {
        case Action.Enable:
            target.SetActive(true);
            break;
        case Action.Disable:
            target.SetActive(false);
            break;
        case Action.Toggle:
            target.SetActive(!target.activeSelf);
            break;
        }

        isComplete = true;
    }

}



