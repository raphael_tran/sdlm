using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Move a GameObject in the UI, following a straight trajectory. */
[AddComponentMenu("Scene Events/Move GameObject")]
public class MoveGameObject : SceneEvent
{

    [Tooltip("The GameObject to move")]
    public RectTransform gameObjectToMove;

    [Tooltip("The target position for the GameObject in its local coordinates")]
    public Vector2 targetPosition;

    [Tooltip("The amount of time required to reach the target position")]
    public float movementDuration;

    public bool waitForCompletion;



    public override void Process() {
        StartCoroutine(Move());

        if (!waitForCompletion) {
            isComplete = true;
        }
    }


    IEnumerator Move() {
        


        if (movementDuration != 0) {

            Vector2 initialPosition = gameObjectToMove.anchoredPosition;

            float t = 0f;
            Vector2 currentPosition;
            
            while (t < 1f) {
                currentPosition.x = Mathf.SmoothStep(initialPosition.x, targetPosition.x, t);
                currentPosition.y = Mathf.SmoothStep(initialPosition.y, targetPosition.y, t);
                
                gameObjectToMove.anchoredPosition = currentPosition;
                yield return null;

                t += Time.deltaTime / movementDuration;
            }

        }
        
        // Teleporting to the exact target position
        gameObjectToMove.anchoredPosition = targetPosition;


        isComplete = true;
    }


}
