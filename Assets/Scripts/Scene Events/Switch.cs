﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * This class enables to play conditional SceneEvents.
 * Use: TODO - See how that works...
 */
[AddComponentMenu("Scene Events/Switch")]
public class Switch : SceneEvent
{

    
    //public static var global;



    
    [Tooltip("")]
    public string switchName;
    
    [Tooltip("The array containing the different options")]
    public SceneEvent[] options;




    protected override void Awake()
    {
        base.Awake();

        options = GetComponentsInChildren<SceneEvent>();    // /!\ options[0] is this
        
        /*
        // Disabling the event in the succession in order for them to be ignored elsewhere
        for (int i = 1; i < succession.Length; i++) {
            succession[i].gameObject.SetActive(false);
        } */

    }


    public override void Process() {
        //int i = /* TODO */;
//        StartCoroutine(PlayOption(i));
    }



    IEnumerator PlayOption(int i) {

        // degueu... remettre le index
        options[i].gameObject.SetActive(true);
        options[i].Process();
        if (options[i].happensInOneFrame) {
            isComplete = true;;
        }
        while (!options[i].isComplete) {
            yield return null;
        }

        isComplete = true;
    }


}
