﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Represents a choice presented to the player in a dialogue window */
// TODO make end event which are played whichever choice is taken
[AddComponentMenu("Scene Events/Dialogue/Choice")]
public class Choice: DialogueEvent
{

    [Tooltip("The text written on the message")][TextArea]
    public string message;

    public bool closeDialogueUIOnEnd;


    [Space(4)]
    [Tooltip("The list of choices proposed with the events they call")]
    public ChoiceOption[] choices;



    protected Button[] buttons;       // An array containing references to the UI buttons of the choice



    protected override void Awake() {
        base.Awake();
        buttons = dialogueUI.choicesUI.GetComponentsInChildren<Button>(true) as Button[];

        dialogueUI.choicesUI.SetActive(true);
    }



    public override void Process() {
        dialogueUI.textDisplay.SetActive(true);
        StartCoroutine(Type(message));
    }



    protected override IEnumerator Type(string sentence) {
        yield return base.Type(sentence);

        SetChoiceButtons(true);
    }



    protected virtual void SetChoiceButtons(bool value) {

        for (int i = 0; i < choices.Length; i++) {
            buttons[i].GetComponentInChildren<Text>().text = choices[i].text;
            StartCoroutine(SetActiveButton(buttons[i], value));

            if (value) {        // Enabling the buttons
                int j = i;  // Required to bypass the delegate bug happening next line otherwise
                buttons[i].onClick.AddListener(delegate {PlayChoiceOption(j); });
                
                /* Selecting the button if it is the only one */
                if (choices.Length == 1) {
                    buttons[i].Select();
                }

            } else {      // Disabling the buttons
                buttons[i].onClick.RemoveAllListeners();

            }

        }
        /*
        GameObject buttonPrototype = choicesGameObject.GetComponentInChildren<Button>(true).gameObject;
        RectTransform t = (RectTransform) buttonPrototype.transform;

        foreach (string choice in choices) {

            // Instanciating the Object on the button
            GameObject button = Object.Instantiate(buttonPrototype, choicesGameObject.transform);

            // Translating it to the right position
            t = (RectTransform) button.transform;
            t.anchoredPosition += verticalSpacing * Vector2.up;

            // Displaying the button with its text
            button.GetComponentInChildren<Text>().text = choice;
            button.SetActive(true);

        */
    }



    void PlayChoiceOption(int choiceNum) {

        SetChoiceButtons(false);
        if (closeDialogueUIOnEnd) {
            dialogueUI.textDisplay.SetActive(false);
        }

        Debug.Log("Playing choice option: " + (choiceNum+1));
        StartCoroutine(WaitForSubEvent(choices[choiceNum].evnt));
    }


    IEnumerator WaitForSubEvent(SceneEvent evnt) {

        if (evnt) {
            evnt.Process();

            if (!evnt.happensInOneFrame) {
                while (!evnt.isComplete) {
                    yield return null;
                }
            }
        }

        isComplete = true;
    }


}





[System.Serializable]
public struct ChoiceOption {
    
    [SerializeField]
    public string text;

    [SerializeField]
    public SceneEvent evnt;
}
