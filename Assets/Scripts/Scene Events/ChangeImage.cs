﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Represents a UI image change.
 * This can for example be used to change a character's face or a background.
 * This can also be used to make an image appear: just set up an Image with no sprite in the Canvas.

    NOTE: I can easily extend this to handle non UI sprites, if necessary
 */
[AddComponentMenu("Scene Events/Change Image")]
public class ChangeImage : SceneEvent
{

    [Space(4)]
    [Tooltip("The GameObject whose Image is changed")]
    public GameObject targetGameObject;

    public Sprite targetImage;



    public override void Process() {        
        targetGameObject.GetComponent<Image>().sprite = targetImage;
        
        isComplete = true;
    }


}
