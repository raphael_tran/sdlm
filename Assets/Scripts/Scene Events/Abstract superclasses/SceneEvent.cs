﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Superclass for everything that constitue an individual event in the scene progression. */
public abstract class SceneEvent : MonoBehaviour
{

    public bool isComplete { get; protected set; }



    [HideInInspector]
    public bool happensInOneFrame;  // Does this SceneEvent is complete itself on the frame it is played?
        // Note: it was added afterward and may not be implemented everywhere...



    /* Awake() is used instead of Start() to ensure this is set up
     * before a ScenePlayer calls Process() in a Start() function. */
    protected virtual void Awake() {}


    /*
     * The method to override to define how the event will unfold.
     * DON'T FORGET TO SET isComplete to true AT THE END. Otherwise, the ScenePlayer can't process further
     */
    public abstract void Process();




}
