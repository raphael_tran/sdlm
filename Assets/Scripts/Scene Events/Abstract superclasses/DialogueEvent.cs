﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Superclass for events happening on a dialogue UI. */
public abstract class DialogueEvent : SceneEvent
{

    [Tooltip("Check this to disable the possibility to skip the typing animation")]
    public bool unskippable;


    protected DialogueUI dialogueUI;    // A reference to the dialogue UI of this Scene
    protected Text text;      // A reference to the Text Component of the text display


    /* The actual typing speed (number of character typed per second) */
    protected float typingSpeed;


    protected override void Awake() {
        base.Awake();

        // Setting up references
        dialogueUI = FindObjectOfType<DialogueUI>();
        text = dialogueUI.textDisplay.GetComponentInChildren<Text>();

        typingSpeed = GlobalData.defaultTypingSpeed;

    }



    /* Progressively type the characters which constitue a sentence. */
    protected virtual IEnumerator Type(string sentence) {

        int indexCharacter = 0;
        float start = Time.time;        // The time at which this sentence starts

        do {

            // Determining the number of characters that should be typed at this moment
            indexCharacter = (int) (typingSpeed * (Time.time - start));

            if ((indexCharacter >= sentence.Length) || (!unskippable && Input.GetButtonDown("Fire1"))) {
                // Displaying the full sentence
                text.text = sentence;
                break;

            } else {
                // Displaying the partial sentence
                text.text = sentence.Substring(0, indexCharacter);
                
            }
            yield return null;

        } while (indexCharacter < sentence.Length);

    }



    /* Display or hide a button, considering the waiting time before appearing or disappearing. */
    protected virtual IEnumerator SetActiveButton(Button button, bool value) {
        button.interactable = false;
        
        yield return new WaitForSeconds(
                value ?
                StandardValues.TIME_BEFORE_MESSAGE_BUTTON:
                StandardValues.MESSAGE_BUTTON_TIME_TO_DISAPPEAR);

        button.gameObject.SetActive(value);
        button.interactable = value;

    }



}
