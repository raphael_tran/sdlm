﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Enables to wait for a certain amount of time before continuing the dialogue sequence. */
[AddComponentMenu("Scene Events/Pause")]
public class Pause : SceneEvent
{

    [Space(4)]
    [Tooltip("The duration of the pause in seconds")]
    public float pauseTime;



    public override void Process() {
        StartCoroutine(PauseDialogue());
    }

    IEnumerator PauseDialogue() {
        yield return new WaitForSeconds(pauseTime);
        isComplete = true;
    }


}
