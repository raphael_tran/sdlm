﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Rescale a GameObject.
 * Note: at the time this is created, its only purpose is to adjust the black screen sprite mask.
 * Note2: eventually, I created SetFocus.cs, so I do not use this class for the moment
 */
[AddComponentMenu("Scene Events/Rescale GameObject")]
public class RescaleGameObject : SceneEvent
{

    [Tooltip("The GameObject to rescale")]
    public Transform gameObjectToRescale;

    [Tooltip("The target scale for the GameObject in its local coordinates")]
    public Vector2 targetScale;

    [Tooltip("The amount of time required to reach the target scale")]
    public float movementDuration;

    public bool waitForCompletion;



    private float initialX, initialY;

    private Vector3 _targetScale;       // An internal variable taking the z coordinate into account



    public override void Process() {
		initialX = gameObjectToRescale.localScale.x;
        initialY = gameObjectToRescale.localScale.y;

        _targetScale = targetScale;
        _targetScale.z = 1;

        StartCoroutine(Rescale());

        if (!waitForCompletion) {
            isComplete = true;
        }
    }


    IEnumerator Rescale() {
        if (movementDuration != 0) {

            float t = 0f;
            Vector3 currentScale;
            currentScale.z = 1;

            while (t < 1f) {
                currentScale.x = Mathf.SmoothStep(initialX, _targetScale.x, t);
                currentScale.y = Mathf.SmoothStep(initialY, _targetScale.y, t);

                gameObjectToRescale.transform.localScale = currentScale;
                yield return null;

                t += Time.deltaTime / movementDuration;
            }

        }

        // Setting the exact target scale
        gameObjectToRescale.localScale = _targetScale;


        isComplete = true;
    }


}
