﻿The current game system is based on the principle of some ScenePlayers which can play a sequence of SceneEvents.
It is quite powerful to build for the visual novel part.
Surprisingly, it ended up looking a bit similar to the RPG Maker flow.



A SceneEvent represents an atomic event which produce a specific impact on the scene.
For example, showing a message, moving a GameObject, playing a sound, etc.
	It is a MonoBehaviour which contains a Process method;
	you need to override it to describe how the event will unfold.
	When complete, a flag "isComplete" is set true to indicates that it ended.

A ScenePlayer is a script to attach to a scene manager GameObject to process the global flow of a scene.
	It notably contains the static Coroutine PlayEventsSequence which can play a succession of SceneEvents.
	There is a StandardScenePlayer which simply plays a main sequence of events before loading a new Scene (see its Documentation for more details)
	It can be derived for a more specific behaviour.


Example of a classic scene using this System:
	A GameObject SceneManager contains a StandardScenePlayer component.
	Another GameObject tagged "Main Events Sequence" has a list of GameObjects with SceneEvent components as children, ordered.
	This list of SceneEvents will be played in the order they are in the Hierarchy.
	Most of them can be obtained from some Prefabs.
	Check out the scene Introduction for a good example.




Contact Raphaël Tran - raphael.tran@telecom-sudparis.eu  for support