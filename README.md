Source code for Les Secrets de la Médiathèque, a serious game aiming to teach
students how to use the digital services provided by the media library of the
engineering school Télécom SudParis.

The game is available to play [here](https://prez-cell.itch.io/gate).

(https://secretsmediatheque.imtbs-tsp.eu/ not active anymore)
